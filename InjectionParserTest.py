from SQLInjection import *

if __name__ == '__main__':

	sqlI = SQLInjection()

	# Test 1
	sample = """select cat from dog where casa=1 ;"""
	u_input =  """select cat from dog where casa=2 ;"""


	print "Test 1"
	print "Sample:  ", sample
	print "User In: ", u_input
	print "Is Valid?: ", sqlI.validateParser(sample, u_input)
	print

	# Test 2

	u_input = """select cat from dog where casa=1 and cat="miau" ;"""
	print "Test 1"
	print "Sample:  ", sample
	print "User In: ", u_input
	print "Is Valid?: ", sqlI.validateParser(sample, u_input)
	print

	# Interactive Example with user input

	print "Follow the instruction and then try to inject SQL."

	while True:
		try: 
			s = raw_input("Input a number> ")
		except EOFError:
			break

		u_input = """select cat from dog where casa=%s ;""" %s
		print "User query: %s" % u_input

		try:
			print "Is Valid?: ", sqlI.validateParser(sample, u_input)
			s_ast, u_ast = sqlI.getLastAsts()
			print "Sample AST: "
			sqlI.print_ast(s_ast)
			print
			print "User   AST: "
			sqlI.print_ast(u_ast)
			print 
		except:
			print "False"




