[English](#markdown-header-grammars-sql-injection) | [Español](#markdown-header-gramaticas-inyeccion-de-sql)

# Gramáticas - Inyección de SQL
##Objetivos

1. Practicar la implementación de gramáticas libres de contexto para construir un analizador de sintaxis de una versión simple de una pregunta de selección de SQL.
 
## Pre-Modulo:

### Bajar PLY
Obten PLY desde: http://www.dabeaz.com/ply/

###Instalar PLY

Descomprime el archivo:

```
tar xzvf ply-X.X.tar.gz
```

Entra al directorio creado:

```
cd ply-X.X
```

Construye los modulos de PLY:

```
python setup.py build
```

Instala PLY en el sistema como administrador:
```
sudo python setup.py install
```

### Pre-Modulo

Antes de trabajar en este módulo el estudiante debe

1. Haber repasado sobre gramáticas libre de contexto.
3. Haber leido sobre el constructor de analizadores sintáctico yacc.

## Prevenir Inyección de SQL usando un analizador sintáctico

La validación de entradas es escencial en el desarrollo de aplicaciones seguras ya que es la primera línea de defensa de todas las aplicaciones.  La gran mayoría de las vulnerabilidades en aplicaciones es debido a pobre validación de las entradas de las aplicaciones. Ejemplo de vulnerabilidades explotadas por pobre validación de entradas lo son:

1. Desbordamiento del Búfer 
2. Cross Site Scripting
3. Inyección de SQL, XML u otros lenguajes
4. Recorrido de directorios
5. Acceso a archivos privados

Los atacantes utilizan errores en la validación de entradas para acceder a información privilegiada, para acceder a los sistemas, o para causar negación de servicios.

Utilizando un analizador sintáctico se puede comparar el árbol sintáctico generado por una pregunta de SQL válida con el tipo de entradas esperadas versus el árbol sintáctico generado por una pregunta de SQL con entradas de usuario y determinar por la diferencia de los árboles si la entrada del usuario es válida.  Si el usuario no entrase el tipo de dato válido o tratara de inyectar SQL en la entrada el árbol sintáctico cambiaría.  Otra ventaja de utilizar un analisador sintáctico es que este te puede avisar si la pregunta que se construyó es sintácticamente válida y por ende, no va a causar un error con el servidor de SQL y quizas hasta romper la aplicación.

Un ejemplo de una pregunta de SQL sencilla es:

```
select * from infoprivada where contrasena="valor aleatorio desconocido" ;
```

Esta pregunta de SQL revelaría la información "privada" de un campo de una tabla si se provee la contraseña correcta.  En la aplicación donde se utiliza esta pregunta la entrada del usuario consiste de insertar la contraseña.  Un posible ataque que revelaría la información privada de toda la tabla sería que usuario insertara como valor la cadena de caracteres `1" or 1=1` que generaría la pregunta: 

```
select * from infoprivada where contrasena="1" or 1=1;
```

En cuyo caso por la lógica de la pregunta la expresión del WHERE es siempre cierta, y por lo tanto revelaría toda la información de la tabla. Para aprender más sobre Inyección de SQL vaya a la referencia[1].

Para propósito de este módulo, el analyzador sintáctico que se va a construir sirve para obtener los árboles sintácticos de una versión simplificada de la pregunta de selección de SQL cuya regla sintáctica inicial es la que sigue:

select : SELECT columns FROM tables WHERE where_expression SEMI

los terminales de la gramática son:

SELECT, FROM, WHERE, AND, OR, NUMBER, ID, LITERAL, LPAREN `(`, RPAREN `)`, EQ, NEQ, GT, LT, GTE, LTE, SEMI, COMMA, WILD `*`, COMMENT

y no permite uniones o expresiones compuestas de preguntas recursivas de SQL.

Por ejemplo:

Para la pregunta de SQL `select * from table where field=%s`, donde %s sería la entrada del usuario; la posible entrada que requiere la aplicación donde se está utilizando puede ser un número insertado por el usuario. 

Como la entrada esperada es un número, una pregunta que se conoce que va a ser válida es `select * from table where field=1`, la cual generaría el árbol sintáctico:

![](images/tree1.jpg)

Si un usuario entrara cualquier otro número lo cual es válido, también se generarían el mismo árbol sintáctico ya que la sintaxis no cambia.

Pero si el usuario en ves de entrar un número ingresara la entrada `1 or 1=1` para que la pregunta siempre sea cierta `select * from table where field=1 or 1=1` el árbol sintáctico que se generaría es:

![](images/tree2.jpg)

que como se puede observar es diferente al árbol válido por que la sintasis cambía.

Otro ejemplo de una entrada inválida sería poner algo diferente a un número como una cadena de caracteres como myfield `select * from table where field=myfield` el árbol sintáctico que se generaría es:

![](images/tree3.jpg)

que como se puede observar el árbol tiene la misma estructura, pero el tipo de uno de los nodos es distinto.

**Descargo de responsabilidad** Esto es un módulo cuyo proposito es enseñar gramáticas libres de contexto y construcción de analizadores sintácticos utilizando una aplicación de seguridad.

## Instrucciones generales

En este módulo utilizaremos la librería de python PLY para construir un analizador sintáctico de preguntas de selección de SQL simples.  El estudiante 
completará la gramática del analizador sintáctico y luego seguirá instrucciones para aprender como el analisis sintáctico sirve para prevenir inyecciones de SQL.

El archivo que el estudiante modificará para llevar a cabo este módulo es:
`SQLParser.py`


## Ejercicio 1: Completar gramática del analizador sintáctico.

Edite el archivo `SQLParser.py` para completar la gramática del analizador sintáctico en las funciones que comienzan con p, como la función inicial `p_select(self, p)`.

1. La gramática para id\_list es un ID o un ID concadenado a un id\_list separado por comas.
2. La gramática para simple_expression es:
    1. a factor or
    2. a factor less than another factor or
    3. a factor greather than another factor,
    4. y así sucesivamente...

## Ejercicio 2: Correr el analizador sintáctico para contestar preguntas

Corra el analizador sintáctico:

```
python  InjectionParserTest.py
```
Van a salir desplegadas dos pruebas donde se comparan una pregunta válida de ejemplo y otra donde las entradas del WHERE es distinta.

1. Note que para la prueba 1, el resultado es válido.  Anote por qué?
2. Note que para la prueba 2, el resultado es inválido.  Anote por qué?
3. Inserte como entrada un número entero cualquiera. Es válido?  Por qué? (Compare los niveles de los árboles)
4. Inserte como entrada `1 or 1=1`.  Es válido? Por qué? 
5. Inserte como entrada `"ccom" or casa=4`.  Es válido? Por qué?

## Ejercicio 3: Un analizador sintáctico para preguntas de SQL para insertar

Modifica el analizador sintáctico de el archivo `SQLParser.py` para también analizar la sintaxis de preguntas de SQL simples como las siguientes: 

insert into table (col1, col2, col3) value (id, 3, "literal") ;
insert into table (col1, col2, col3) values (id, 3,"literal"), (id, 4, "otherliteral") ;

**Nota** Crea una copia de el archivo `SQLParser.py` del Ejercicio 1 porque necesitaras entregarlo al instructor con otra copia del `SQLParser.py` para este ejercicio. 

## Entregas

Entregue al instructor una copia del archivo `SQLParser.py` del Ejercicio 1 y el Ejercicio 3, y las contestaciones a las preguntas del Ejercicio 2.

## References:
[1] https://en.wikipedia.org/wiki/SQL_injection

--- 

[English](#markdown-header-grammars-sql-injection) | [Español](#markdown-header-gramaticas-inyeccion-de-sql)

# Grammars - SQL Injection

##Objectives

1. Practice the implementation of context free grammars to construct a syntax analyzer of a simple version of a SQL select query.
 
## Pre-Module:

### Download PLY
Get PLY from: http://www.dabeaz.com/ply/

###Install PLY

Uncompress the file:

```
tar xzvf ply-X.X.tar.gz
```

Enter the directory created:

```
cd ply-X.X
```

Construct the PLY modules:

```
python setup.py build
```

Install PLY in the system as an administrator:

```
sudo python setup.py install
```

### Pre-Module

Before working in this module the student must

1. Have reviewed about context free grammars.
3. Have read about the constructor of syntax analyzers yacc.

## Prevention SQL injection using a syntax analyzer

Input validation is esential in the development of secure applications because it is the first line of defense of every application.  The vast majority of vulnerabilities in applications is due to poor input validation of the applications.  Example of vulnerabilities explited by poor input validation are:

1. Buffer overflows
2. Cross Site Scripting
3. SQL, XML or other languages injection
4. Directory Traversal
5. Access to private files

The attackers use errors in input validation to gain access to priviledged information, to gain access to systems, or to cause denial of services.

Using a syntax analyzer we can compare the syntax tree generated by a valid SQL query with expected inputs types versus the syntax tree generated by a SQL query with user input and determine with the difference of the trees if the user input is valid. If the user input is not of a valid type or if the user tries to inject SQL the syntax tree will change.  Another advantage of using a syntax analyzer is that it can warn if the constructed query is syntactically valid, and thus, it would not cause an error with the SQL server and perhaps even break the application.

An example of a simple SQL query is:

```
select * from infoprivada where contrasena="valor aleatorio desconocido" ;
```

This SQL query would reveal "private" information of a field of the a table if the right password is provided.  In the aplication where this query would be used the user input would consist of inserting the password.  A possible atack that would reveal the private information of all the table would be that the user inserts as a value the string `1" or 1=1` that would generate the query:

```
select * from infoprivada where contrasena="1" or 1=1;
```

In which case because of the logic of the query WHERE expression is always true, and therefore it would reveal all the information in the table.  To learn more about SQL injection go to reference [1].

For the purpose of this module, the syntax analyzer that will be constructed will serve to obtain the syntax trees of a simplified version of the select SQL query which init syntax rules is:

select : SELECT columns FROM tables WHERE where_expression SEMI

the grammar terminals are:

SELECT, FROM, WHERE, AND, OR, NUMBER, ID, LITERAL, LPAREN `(`, RPAREN `)`, EQ, NEQ, GT, LT, GTE, LTE, SEMI, COMMA, WILD `*`, COMMENT

and does not permits UNIONs, or expressions composed of recursive SQL queries.

For example:

For the SQL query `select * from table where field=%s`, where %s would be the user input; the possible input requiered by the application where the query is being used could be a number inserted by the user.

Because the expected input is a number, a query known to be valid is `select * from table where field=1`, which would generate the syntax tree:

![](images/tree1.jpg)

If the user enters any other number, which is valid, also the same syntax tree would be generated because the syntax tree does not changes.

But if the user instead of inserting a number inserts as input `1 or 1=1` to make the query always true `select * from table where field=1 or 1=1` the syntax tree that would be generated is:

![](images/tree2.jpg)

that as can be observed is different to the valid tree because the syntax changes.

Another example of an invalid input would be to insert something different to a number such a string like myfield `select * from table where field=myfield` where the generated syntax tree is:

![](images/tree3.jpg)

that as can be observed it is different to the valid syntax tree because the syntax changes

**Disclaimer** This is a module which purpose is to teach context free grammars and to construct syntax analyzers using security as an application.

## General Instructions

In this module we will use the python library PLY to construct a syntax analyzer of simple selection SQL queries.  The student will complete the grammar of the syntax analizer and then will follow instructions to learn how the syntax analyisis serves to prevent SQL injections.

The file that the student will modify to accomplish this module is: `SQLParser.py`


## Exercise 1: Complete the grammar of the syntax analyzer

Edit the file  `SQLParser.py` to complete the grammar of the syntax analyzer in the functions that start with `p`, like the initial function `p_select(self, p)`.

1. The grammar for id\_list is an ID or an ID concatenated to an id\_list separated by commas.
2. The grammar for simple_expression is:
    1. a factor or
    2. a factor less than another factor or
    3. a factor greather than another factor,
    4. and so on...

## Exercise 2: Run the syntax analyzer and answer the questions

Run the syntax analyzer:

```
python  InjectionLexTest.py
```
The comparison between two tests, one with a sample valid query and other with a different WHERE expressions input is displayed.

1. Note that for test 1, the result is valid.  Why?
2. Note that for test 2, the result is invalid. Why?
3. Insert as input any integer number.  Is valid? Why? (Compare the levels of the syntax tree)
4. Insert as input `1 or 1=1`. Is valid? Why?
5. Insert as input `"ccom" or casa=4`. Is valid? Why?
``

## Exercise 3: A syntax analyzer for insert queries

Modify the syntax analyzer of file `SQLParser.py` to also analyze the syntax of simple insert SQL queries like the following:

insert into table (col1, col2, col3) value (id, 3, "literal") ;
insert into table (col1, col2, col3) values (id, 3,"literal"), (id, 4, "otherliteral") ;

**Note** make a copy of the file `SQLParser.py` from Exercise 1 because you need to give it to the instructor with another `SQLParser.py` for this exercise. 

## Deliverables

Give the instructor a copy of the file `SQLParser.py` from Exercise 1, and Exercise 3; and the answers to the questions from Exercise 2.

## References:
[1] https://en.wikipedia.org/wiki/SQL_injection

